<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <title>Material Design Bootstrap</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" />
  <!-- Bootstrap core CSS -->
  <link href="<?php echo $this->config->item('skillll'); ?>css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?php echo $this->config->item('skillll'); ?>css/select2.min.css" rel="stylesheet" />

  <!-- file input -->
  <link href="<?php echo $this->config->item('skillll'); ?>dist/bootstrap-fileinput/css/fileinput.min.css" media="all"
  rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
 
  <!-- Material Design Bootstrap -->
  <link href="<?php echo $this->config->item('skillll'); ?>css/mdb.css" rel="stylesheet" />
  <!-- Your custom styles (optional) -->
  <link href="<?php echo $this->config->item('skillll'); ?>dist/css/style.css" rel="stylesheet" />
  
</head>


<body>

  <!--Main Navigation-->
  <header>
      <!-- Navbar -->
      <nav
        class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar"
      >
        <div class="container">
          <!-- Brand -->
          <a
            class="navbar-brand waves-effect"
            href="https://mdbootstrap.com/docs/jquery/"
            target="_blank"
          >
            <img src="<?php echo $this->config->item('skillll'); ?>img/logo.png" alt="skillll" />
          </a>

          <!-- Collapse -->
          <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>

          <!-- Links -->
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left -->
            <ul class="navbar-nav mr-auto m-auto pl-10">
              <li class="nav-item ">
                <div class="form-group has-search">
                  <span class="fas fa-search form-control-feedback"></span>
                  <input
                    type="text"
                    class="form-control"
                    placeholder="search skillll"
                  />
                </div>
              </li>
            </ul>

            <!-- Right -->
            <ul class="navbar-nav nav-flex-icons">
              <li class="nav-item">
                <a
                  href="https://www.facebook.com/mdbootstrap"
                  class="nav-link waves-effect"
                  target="_blank"
                >
                  <img src="<?php echo $this->config->item('skillll'); ?>img/icon/01.png" />
                </a>
              </li>
              <li class="nav-item">
                <a
                  href="https://twitter.com/MDBootstrap"
                  class="nav-link waves-effect"
                  target="_blank"
                >
                  <img src="<?php echo $this->config->item('skillll'); ?>img/icon/02.png" />
                </a>
              </li>
              <li class="nav-item">
                <a
                  href="https://github.com/mdbootstrap/bootstrap-material-design"
                  class="nav-link  waves-effect"
                >
                  <img src="<?php echo $this->config->item('skillll'); ?>img/icon/03.png" />
                </a>
              </li>
              <li class="nav-item">
                <a
                  href="https://github.com/mdbootstrap/bootstrap-material-design"
                  class="nav-link   waves-effect"
                >
                  <img class="point" src="<?php echo $this->config->item('skillll'); ?>img/icon/04.png" />
                  <span>150</span>
                </a>
              </li>
              <li class="nav-item">
                <img class="user" src="<?php echo $this->config->item('skillll'); ?>img/icon/user.png" />
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- Navbar -->
    </header>
  <!--Main Navigation-->

  <!--Main layout-->
  <main class="mt-3 pt-5">
    <div class="container">

      <!--Section: Post-->
      <section class="mt-4">

        <!--Grid row-->
        <div class="row">   

          <!--Grid column-->
          <div class="col-xs-3 col-sm-3 col-md-4 col-lg-3 mb-12 pc">

            <!--Card: Jumbotron-->
            <section class="text-center wow fadeIn">
                <div class="card my-radius ">
                  <!--Card image-->
                  <div class="text-left">
                    <img class="p-3" src="<?php echo $this->config->item('skillll'); ?>img/icon/user2.png" />
                    <div class="grid-line">
                      <span class="text-name">Gorra Design</span>
                      <span class="text-name-sub">@Gorradesign</span>
                    </div>
                  </div>
            
                  <!--Card content-->
                  <div class="card-body">
                    <!--Title-->
                    <h4 class="text-name-lable ">Student</h4>
                    <div class="row text-name-lable">
                      <div class="col-6"><span class="text-info">2</span></div>
                      <div class="col-6"><span class="text-info">1,200</span></div>
                    </div>
                    <div class="row text-name-lable">
                      <div class="col-6">Classes</div>
                      <div class="col-6">Following</div>
                    </div>
                    <h4 class="text-name-lable mt-3">Teacher</h4>
                    <div class="row text-name-lable">
                      <div class="col-6"><span class="text-info">5</span></div>
                      <div class="col-6"><span class="text-info">50,995</span></div>
                    </div>
                    <div class="row text-name-lable">
                      <div class="col-6">Classes</div>
                      <div class="col-6">Following</div>
                    </div>
          
                  </div>
                </div>
            </section>
            <!--Card: Jumbotron-->

            <!--Card : Dynamic content wrapper-->
            <section class="text-center wow fadeIn mt-3">
                <div class="card my-radius ">
                  <!--Card content-->
                  <div class="card-body">
                    <div class="text-left mb-3">
                      <h4 class="text-name">Purchased Classes</h4>
                    </div>
                    <!--Title-->
                    <div class="mb-3">
                      <span class="text-name-lable float-left">Furniture Class</span>
                      <img class="img-fluid" src="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png" alt="skilll" srcset="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png">
                    </div>
                    <div class="mb-3">
                      <span class="text-name-lable float-left">Plant Class</span>
                      <img class="img-fluid" src="<?php echo $this->config->item('skillll'); ?>img/demo/img02.png" alt="skilll" srcset="<?php echo $this->config->item('skillll'); ?>img/demo/img02.png">
                    </div>

                    <a href="#">See All</a>
            
                  </div>
                </div>
            </section>
            <!--/.Card : Dynamic content wrapper-->

            <!--Card : Dynamic content wrapper-->
            <section class="text-left wow fadeIn mt-3 pc mobile popular">
              <div class="card my-radius ">
                <!--Card content-->
                <div class="card-body">
                  <div class="text-left mb-3">
                    <h4 class="text-name text-info">Popular Classes</h4>
                  </div>
                  <!--Title-->
                  <div class="mb-3">
                    <img class="img-fluid" src="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png" alt="skilll" srcset="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png">
                      <div class="float-left width-60"><span class="text-name-lable small-text ">Drawing Basics Class
                      By : Gorradesign</span></div> 
                      <div class="float-right">
                        <span class="text-name-lable small-text">1,200</span>
                        <img src="<?php echo $this->config->item('skillll'); ?>img/icon/group.png" >
                      </div>
                      <div class="clearfix"></div>
                  </div>    
                   
                  <div class="mb-3">
                    <img class="img-fluid" src="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png" alt="skilll" srcset="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png">
                    <div class="float-left width-60"><span class="text-name-lable small-text ">Drawing Basics Class
                        By : Gorradesign</span></div>
                    <div class="float-right">
                      <span class="text-name-lable small-text">1,200</span>
                      <img src="<?php echo $this->config->item('skillll'); ?>img/icon/group.png">
                    </div>
                    <div class="clearfix"></div>
                  </div>

                  <div class="mb-3">
                    <img class="img-fluid" src="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png" alt="skilll" srcset="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png">
                    <div class="float-left width-60"><span class="text-name-lable small-text ">Drawing Basics Class
                        By : Gorradesign</span></div>
                    <div class="float-right">
                      <span class="text-name-lable small-text">1,200</span>
                      <img src="<?php echo $this->config->item('skillll'); ?>img/icon/group.png">
                    </div>
                    <div class="clearfix"></div>
                  </div>


                  <div class="text-center">
                  <a href="#">See All</a>
                  </div>
            
                </div>
              </div>
            </section>
            <!--/.Card : Dynamic content wrapper-->
            
          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-xs-6 col-sm-6 col-md-8 col-lg-6 mb-12">
          
            <!--Featured Post-->
            <div class=" mb-4 wow input-post-mini">
              <div class="form-group has-search ">
                <img class="p-3 form-control-feedback" src="<?php echo $this->config->item('skillll'); ?>img/icon/user2.png">
                <input  type="text" class="form-control post" placeholder="Post something.." >
              </div>
            </div>
            <!--/.Featured Post-->
          
            <!--Card-->
            <div class="card mb-4 wow" id="post-sec">
          
              <!--Card content-->
              <div class="card-body text-left">
                <button type="button" class="close" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
                <h5 class="text-name-lable text-left" id="postLabel">Create your Post</h5>
                <div class="form-group has-search mb-3">
                  <img class="p-3 form-control-feedback" src="<?php echo $this->config->item('skillll'); ?>img/icon/user2.png">
                  <textarea rows="1" type="text" id="input-post" class="form-control post-content" placeholder="Post something.."></textarea>
                </div>
              
                <div class="form-group">
                  <div class="file-loading">
                    <input type="file" class="file" id="test-upload" multiple data-theme="fas">
                  </div>
                  <div id="errorBlock" class="help-block"></div>
                </div>

                <div class="form-group col-12">
                  <div class="row">
                    <div class="col">
                      <select class="select-post">
                          <option>Nested option</option>
                          <option>Nested option</option>
                          <option>Nested option</option>
                          <option>Nested option Nested option Nested option Nested option</option>
                      </select>
                    </div>
                  <div class="col">
                    <button type="button" class="btn btn-primary post float-right">Post</button>
                  </div>
                  </div>
                  
                </div>
              
              </div>
          
            </div>
            <!--/.Card-->
          
            <!--Card-->
            <div class="card mb-4 wow fadeIn">
              <!--Card content-->
              <div class="card-body">
          
                <div class="media d-block d-md-flex ">
                  <img class="mb-3 mx-auto z-depth-1 rounded-circle" src="https://mdbootstrap.com/img/Photos/Avatars/img (30).jpg"
                    alt="Generic placeholder image" style="width: 60px;">
                  <div class="media-body text-md-left ml-md-3 ml-0">
                    <h5 class="mt-0 font-weight-bold">Mr.Winson
                    </h5>
                    <p>I will show you how to build the easy chair in your home...</p>
                    <img src="<?php echo $this->config->item('skillll'); ?>img/icon/public.png"><span class="text-name-lable ml-2">6 days ago</span>
                  </div>
                </div>
                <div class="post-contents">
                <iframe src="https://player.vimeo.com/video/76979871?embedparameter=value" width="100%" height="" frameborder="0"
                  allowfullscreen></iframe>
                </div>
                <div class="post-contents-detail p-2">
                  <div class="row">
                  <div class="col-3 ">
                  <img src="<?php echo $this->config->item('skillll'); ?>img/demo/shutterstock.png">
                  </div>
                  <div class="col-6">
                    <p class="fri">Furniture DIY course make it at home</p>
                    <p class="sec text-name-lable">He is questioned by U.S. customs officials,
                    and his scars and gang tattoos, marking him</p>
                  </div>
                  <div class="col-3">
                    <button class="btn btn-orange-c post">Free</button>
                  </div>
                  </div>
                </div>

                <div class="post-contents-comment p-2 mt-2">
                  <div class="row">
                    <div class="col-3 pr-0">
                      <span class="">Views 1,986</span>
                    </div>
                    <div class="col-2 px-0">
                      <img src="<?php echo $this->config->item('skillll'); ?>img/icon/07.png"><span class="pl-1">150</span>
                    </div>
                    <div class="col-2 px-0">
                      <img src="<?php echo $this->config->item('skillll'); ?>img/icon/06.png"><span class="pl-1">20</span>
                    </div>
                    <div class="col-2 px-0">
                      <img src="<?php echo $this->config->item('skillll'); ?>img/icon/05.png"><span class="pl-1">Share</span>
                    </div>
                    <div class="col-2 px-0">
                      <button class="btn btn-info post m-0">Follow</button>
                    </div>
                  </div>

                  <div class="row mt-2">
                    <div class="col-1 mr-2">
                      <img class="rounded-circle" src="https://mdbootstrap.com/img/Photos/Avatars/img (30).jpg"
                        alt="Generic placeholder image" style="width: 40px;">
                    </div>
                    <div class="col-6">
                      <input type="text" class="form-control comment">
                    </div>
                    <div class="col-4 mt-2">
                      <a href="#">Send</a>
                    </div>
                  </div>
                  <!-- comment -->
                  <div class="row mt-2">
                    <div class="col-1 mr-2">
                      <img class="rounded-circle" src="https://mdbootstrap.com/img/Photos/Avatars/img (30).jpg"
                        alt="Generic placeholder image" style="width: 40px;">
                    </div>
                    <div class="col-10 comment-text">
                      <div class="row ml-2">
                      He is questioned by U.S. customs officials, and his scars and gang tattoos, marking him as an assassin, do not go
                      unnoticed.
                      </div>
                      <div class="row mt-1">
                        <div class="col-2 px-0 like">
                          <span class="pl-1">3</span>
                          <img src="<?php echo $this->config->item('skillll'); ?>img/icon/07.png">
                        </div>
                        <div class="col-2 px-0 ">
                          6 days ago
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end comment -->
                  <!-- comment -->
                  <div class="row mt-2">
                    <div class="col-1 mr-2">
                      <img class="rounded-circle" src="https://mdbootstrap.com/img/Photos/Avatars/img (30).jpg"
                        alt="Generic placeholder image" style="width: 40px;">
                    </div>
                    <div class="col-10 comment-text">
                      <div class="row ml-2">
                        He is questioned by U.S. customs officials, and his scars and gang tattoos, marking him as an assassin, do not go
                        unnoticed.
                      </div>
                      <div class="row mt-1">
                        <div class="col-2 px-0 like">
                          <span class="pl-1">3</span>
                          <img src="<?php echo $this->config->item('skillll'); ?>img/icon/07.png">
                        </div>
                        <div class="col-2 px-0 ">
                          6 days ago
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end comment -->
                </div>
          
              </div>
          
            </div>
            <!--/.Card-->

             <!--Card-->
            <div class="card mb-4 wow fadeIn">
              <!--Card content-->
              <div class="card-body">
          
                <div class="media d-block d-md-flex ">
                  <img class="mb-3 mx-auto z-depth-1 rounded-circle" src="https://mdbootstrap.com/img/Photos/Avatars/img (30).jpg"
                    alt="Generic placeholder image" style="width: 60px;">
                  <div class="media-body text-md-left ml-md-3 ml-0">
                    <h5 class="mt-0 font-weight-bold">Mr.Winson
                    </h5>
                    <p>I will show you how to build the easy chair in your home...</p>
                    <img src="<?php echo $this->config->item('skillll'); ?>img/icon/public.png"><span class="text-name-lable ml-2">6 days ago</span>
                  </div>
                </div>
                <div class="post-contents">
                <iframe src="https://player.vimeo.com/video/76979871?embedparameter=value" width="100%" height="" frameborder="0"
                  allowfullscreen></iframe>
                </div>
                <div class="post-contents-detail p-2">
                  <div class="row">
                  <div class="col-3 ">
                  <img src="<?php echo $this->config->item('skillll'); ?>img/demo/shutterstock.png">
                  </div>
                  <div class="col-6">
                    <p class="fri">Furniture DIY course make it at home</p>
                    <p class="sec text-name-lable">He is questioned by U.S. customs officials,
                    and his scars and gang tattoos, marking him</p>
                  </div>
                  <div class="col-3">
                    <button class="btn btn-orange-c post">Free</button>
                  </div>
                  </div>
                </div>

                <div class="post-contents-comment p-2 mt-2">
                  <div class="row">
                    <div class="col-3 pr-0">
                      <span class="">Views 1,986</span>
                    </div>
                    <div class="col-2 px-0">
                      <img src="<?php echo $this->config->item('skillll'); ?>img/icon/07.png"><span class="pl-1">150</span>
                    </div>
                    <div class="col-2 px-0">
                      <img src="<?php echo $this->config->item('skillll'); ?>img/icon/06.png"><span class="pl-1">20</span>
                    </div>
                    <div class="col-2 px-0">
                      <img src="<?php echo $this->config->item('skillll'); ?>img/icon/05.png"><span class="pl-1">Share</span>
                    </div>
                    <div class="col-2 px-0">
                      <button class="btn btn-info post m-0">Follow</button>
                    </div>
                  </div>

                  <div class="row mt-2">
                    <div class="col-1 mr-2">
                      <img class="rounded-circle" src="https://mdbootstrap.com/img/Photos/Avatars/img (30).jpg"
                        alt="Generic placeholder image" style="width: 40px;">
                    </div>
                    <div class="col-6">
                      <input type="text" class="form-control comment">
                    </div>
                    <div class="col-4 mt-2">
                      <a href="#">Send</a>
                    </div>
                  </div>
                  <!-- comment -->
                  <div class="row mt-2">
                    <div class="col-1 mr-2">
                      <img class="rounded-circle" src="https://mdbootstrap.com/img/Photos/Avatars/img (30).jpg"
                        alt="Generic placeholder image" style="width: 40px;">
                    </div>
                    <div class="col-10 comment-text">
                      <div class="row ml-2">
                      He is questioned by U.S. customs officials, and his scars and gang tattoos, marking him as an assassin, do not go
                      unnoticed.
                      </div>
                      <div class="row mt-1">
                        <div class="col-2 px-0 like">
                          <span class="pl-1">3</span>
                          <img src="<?php echo $this->config->item('skillll'); ?>img/icon/07.png">
                        </div>
                        <div class="col-2 px-0 ">
                          6 days ago
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end comment -->
                  <!-- comment -->
                  <div class="row mt-2">
                    <div class="col-1 mr-2">
                      <img class="rounded-circle" src="https://mdbootstrap.com/img/Photos/Avatars/img (30).jpg"
                        alt="Generic placeholder image" style="width: 40px;">
                    </div>
                    <div class="col-10 comment-text">
                      <div class="row ml-2">
                        He is questioned by U.S. customs officials, and his scars and gang tattoos, marking him as an assassin, do not go
                        unnoticed.
                      </div>
                      <div class="row mt-1">
                        <div class="col-2 px-0 like">
                          <span class="pl-1">3</span>
                          <img src="<?php echo $this->config->item('skillll'); ?>img/icon/07.png">
                        </div>
                        <div class="col-2 px-0 ">
                          6 days ago
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end comment -->
                </div>
          
              </div>
          
            </div>
            <!--/.Card-->
          
            
          
          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-xs-3 col-sm-3 col-md-4 col-md-offset-4 col-lg-3 mb-12 ipad mobile">
          
            <!--Card : Dynamic content wrapper-->
            <section class="text-left wow fadeIn">
              <div class="card my-radius ">
                <!--Card content-->
                <div class="card-body">
                  <div class="text-left mb-3">
                    <h4 class="text-name text-info">Popular Classes</h4>
                  </div>
                  <!--Title-->
                  <div class="mb-3">
                    <img class="img-fluid" src="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png" alt="skilll" srcset="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png">
                      <div class="float-left width-60"><span class="text-name-lable small-text ">Drawing Basics Class
                      By : Gorradesign</span></div> 
                      <div class="float-right">
                        <span class="text-name-lable small-text">1,200</span>
                        <img src="<?php echo $this->config->item('skillll'); ?>img/icon/group.png" >
                      </div>
                      <div class="clearfix"></div>
                  </div>    
                   
                  <div class="mb-3">
                    <img class="img-fluid" src="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png" alt="skilll" srcset="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png">
                    <div class="float-left width-60"><span class="text-name-lable small-text ">Drawing Basics Class
                        By : Gorradesign</span></div>
                    <div class="float-right">
                      <span class="text-name-lable small-text">1,200</span>
                      <img src="<?php echo $this->config->item('skillll'); ?>img/icon/group.png">
                    </div>
                    <div class="clearfix"></div>
                  </div>

                  <div class="mb-3">
                    <img class="img-fluid" src="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png" alt="skilll" srcset="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png">
                    <div class="float-left width-60"><span class="text-name-lable small-text ">Drawing Basics Class
                        By : Gorradesign</span></div>
                    <div class="float-right">
                      <span class="text-name-lable small-text">1,200</span>
                      <img src="<?php echo $this->config->item('skillll'); ?>img/icon/group.png">
                    </div>
                    <div class="clearfix"></div>
                  </div>


                  <div class="text-center">
                  <a href="#">See All</a>
                  </div>
            
                </div>
              </div>
            </section>
            <!--/.Card : Dynamic content wrapper-->
        

        </div>
        <!--Grid row-->

      </section>
      <!--Section: Post-->

    </div>
  </main>
  <!--Main layout-->

  <!--Footer-->
  <footer class="page-footer text-center font-small mdb-color darken-2 mt-4 wow fadeIn">

  </footer>
  <!--/.Footer-->

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="<?php echo $this->config->item('skillll'); ?>js/jquery-3.4.0.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo $this->config->item('skillll'); ?>js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo $this->config->item('skillll'); ?>js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo $this->config->item('skillll'); ?>js/mdb.min.js"></script>
  <!-- Initializations -->
  <script type="text/javascript" src="<?php echo $this->config->item('skillll'); ?>js/select2.full.min.js"></script>

  <script src="<?php echo $this->config->item('skillll'); ?>dist/bootstrap-fileinput/js/plugins/piexif.js" type="text/javascript"></script>
  <script src="<?php echo $this->config->item('skillll'); ?>dist/bootstrap-fileinput/js/plugins/sortable.js" type="text/javascript"></script>
  <script src="<?php echo $this->config->item('skillll'); ?>dist/bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
  <script src="<?php echo $this->config->item('skillll'); ?>dist/bootstrap-fileinput/js/locales/fr.js" type="text/javascript"></script>
  <script src="<?php echo $this->config->item('skillll'); ?>dist/bootstrap-fileinput/js/locales/es.js" type="text/javascript"></script>
  <script src="<?php echo $this->config->item('skillll'); ?>dist/bootstrap-fileinput/themes/fas/theme.js" type="text/javascript"></script>
  <script src="<?php echo $this->config->item('skillll'); ?>dist/bootstrap-fileinput/themes/explorer-fas/theme.js" type="text/javascript"></script>
 
  <script type="text/javascript">
    // Animations initialization
    new WOW().init();
    
    $('#post-sec').hide()
    $(".post").click(function () {
      $('#post-sec').show();
      $('.input-post-mini').hide()
      $('#input-post').focus();
    });

    $('#post-sec .close').click(function(){
      $('#post-sec').hide()
      $('.input-post-mini').show()
    })

    var textarea = document.querySelector('textarea');

      textarea.addEventListener('keypress', autosize);

      function autosize() {
        var el = this;
        var h = el.scrollHeight;
        setTimeout(function () {
          el.style.cssText = 'height:auto; padding:0';
          el.style.cssText = 'height:' + h + 'px';
        }, 0);
      }

    $('.select-post').select2({
      placeholder: "Select tag course",
      allowClear: false
    });  


     var $el1 = $("#test-upload");
      $el1.fileinput({
        theme: 'fas',
        allowedFileExtensions: ['jpg', 'png', 'gif'],
        allowedFileTypes: ['image', 'video', 'flash'],
        uploadUrl: "/file-upload-batch/2",
        uploadAsync: false,
        showCaption: false,
        showUpload: false, // hide upload button
        showRemove: false, // hide remove button
        overwriteInitial: false, // append files to initial preview
        //minFileCount: 1,
        maxFileCount: 5,
        initialPreviewAsData: true,
        browseOnZoneClick: true,
        showBrowse: false,
        previewClass: "bg-light-c",
        dropZoneTitle : "+",
        dropZoneClickTitle : "<br><small>Add Photo or Video</<small>",
        uploadExtraData: function (previewId, index) {
          return { key: index };
        },
      }).on("filebatchselected", function (event, files) {
        $el1.fileinput("upload");
      });
     


  </script>
</body>

</html>
