<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <title>Material Design Bootstrap</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" />
  <!-- Bootstrap core CSS -->
  <link href="<?php echo $this->config->item('skillll'); ?>css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?php echo $this->config->item('skillll'); ?>css/select2.min.css" rel="stylesheet" />

  <!-- file input -->
  <link href="<?php echo $this->config->item('skillll'); ?>dist/bootstrap-fileinput/css/fileinput.min.css" media="all"
  rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
 
  <!-- Material Design Bootstrap -->
  <link href="<?php echo $this->config->item('skillll'); ?>css/mdb.css" rel="stylesheet" />
  <!-- Your custom styles (optional) -->
  <link href="<?php echo $this->config->item('skillll'); ?>dist/css/style.css" rel="stylesheet" />
  
</head>


