<div class="col-xs-3 col-sm-3 col-md-4 col-lg-3 mb-12 pc">

            <!--Card: Jumbotron-->
            <section class="text-center wow fadeIn">
                <div class="card my-radius ">
                  <!--Card image-->
                  <div class="text-left">
                    <img class="p-3" src="<?php echo $this->config->item('skillll'); ?>img/icon/user2.png" />
                    <div class="grid-line">
                      <span class="text-name">Gorra Design</span>
                      <span class="text-name-sub">@Gorradesign</span>
                    </div>
                  </div>
            
                  <!--Card content-->
                  <div class="card-body">
                    <!--Title-->
                    <h4 class="text-name-lable ">Student</h4>
                    <div class="row text-name-lable">
                      <div class="col-6"><span class="text-info">2</span></div>
                      <div class="col-6"><span class="text-info">1,200</span></div>
                    </div>
                    <div class="row text-name-lable">
                      <div class="col-6">Classes</div>
                      <div class="col-6">Following</div>
                    </div>
                    <h4 class="text-name-lable mt-3">Teacher</h4>
                    <div class="row text-name-lable">
                      <div class="col-6"><span class="text-info">5</span></div>
                      <div class="col-6"><span class="text-info">50,995</span></div>
                    </div>
                    <div class="row text-name-lable">
                      <div class="col-6">Classes</div>
                      <div class="col-6">Following</div>
                    </div>
          
                  </div>
                </div>
            </section>
            <!--Card: Jumbotron-->

            <!--Card : Dynamic content wrapper-->
            <section class="text-center wow fadeIn mt-3">
                <div class="card my-radius ">
                  <!--Card content-->
                  <div class="card-body">
                    <div class="text-left mb-3">
                      <h4 class="text-name">Purchased Classes</h4>
                    </div>
                    <!--Title-->
                    <div class="mb-3">
                      <span class="text-name-lable float-left">Furniture Class</span>
                      <img class="img-fluid" src="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png" alt="skilll" srcset="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png">
                    </div>
                    <div class="mb-3">
                      <span class="text-name-lable float-left">Plant Class</span>
                      <img class="img-fluid" src="<?php echo $this->config->item('skillll'); ?>img/demo/img02.png" alt="skilll" srcset="<?php echo $this->config->item('skillll'); ?>img/demo/img02.png">
                    </div>

                    <a href="#">See All</a>
            
                  </div>
                </div>
            </section>
            <!--/.Card : Dynamic content wrapper-->

            <!--Card : Dynamic content wrapper-->
            <section class="text-left wow fadeIn mt-3 pc mobile popular">
              <div class="card my-radius ">
                <!--Card content-->
                <div class="card-body">
                  <div class="text-left mb-3">
                    <h4 class="text-name text-info">Popular Classes</h4>
                  </div>
                  <!--Title-->
                  <div class="mb-3">
                    <img class="img-fluid" src="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png" alt="skilll" srcset="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png">
                      <div class="float-left width-60"><span class="text-name-lable small-text ">Drawing Basics Class
                      By : Gorradesign</span></div> 
                      <div class="float-right">
                        <span class="text-name-lable small-text">1,200</span>
                        <img src="<?php echo $this->config->item('skillll'); ?>img/icon/group.png" >
                      </div>
                      <div class="clearfix"></div>
                  </div>    
                   
                  <div class="mb-3">
                    <img class="img-fluid" src="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png" alt="skilll" srcset="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png">
                    <div class="float-left width-60"><span class="text-name-lable small-text ">Drawing Basics Class
                        By : Gorradesign</span></div>
                    <div class="float-right">
                      <span class="text-name-lable small-text">1,200</span>
                      <img src="<?php echo $this->config->item('skillll'); ?>img/icon/group.png">
                    </div>
                    <div class="clearfix"></div>
                  </div>

                  <div class="mb-3">
                    <img class="img-fluid" src="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png" alt="skilll" srcset="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png">
                    <div class="float-left width-60"><span class="text-name-lable small-text ">Drawing Basics Class
                        By : Gorradesign</span></div>
                    <div class="float-right">
                      <span class="text-name-lable small-text">1,200</span>
                      <img src="<?php echo $this->config->item('skillll'); ?>img/icon/group.png">
                    </div>
                    <div class="clearfix"></div>
                  </div>


                  <div class="text-center">
                  <a href="#">See All</a>
                  </div>
            
                </div>
              </div>
            </section>
            <!--/.Card : Dynamic content wrapper-->
            
          </div>