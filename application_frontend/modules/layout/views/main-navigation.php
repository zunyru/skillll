<!--Main Navigation-->
<header>
	<!-- Navbar -->
	<nav
		class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar"
	>
		<div class="container">
			<!-- Brand -->
			<a
				class="navbar-brand waves-effect"
				href="https://mdbootstrap.com/docs/jquery/"
				target="_blank"
			>
				<img
					src="<?php echo $this->config->item('skillll'); ?>img/logo.png"
					alt="skillll"
				/>
			</a>

			<!-- Collapse -->
			<button
				class="navbar-toggler"
				type="button"
				data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent"
				aria-expanded="false"
				aria-label="Toggle navigation"
			>
				<span class="navbar-toggler-icon"></span>
			</button>

			<!-- Links -->
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<!-- Left -->
				<ul class="navbar-nav mr-auto m-auto pl-10">
					<li class="nav-item ">
						<div class="form-group has-search">
							<span class="fas fa-search form-control-feedback"></span>
							<input
								type="text"
								class="form-control"
								placeholder="search skillll"
							/>
						</div>
					</li>
				</ul>

				<!-- Right -->
				<ul class="navbar-nav nav-flex-icons">
					<li class="nav-item">
						<a
							href="https://www.facebook.com/mdbootstrap"
							class="nav-link waves-effect"
							target="_blank"
						>
							<img
								src="<?php echo $this->config->item('skillll'); ?>img/icon/01.png"
							/>
						</a>
					</li>
					<li class="nav-item">
						<a
							href="https://twitter.com/MDBootstrap"
							class="nav-link waves-effect"
							target="_blank"
						>
							<img
								src="<?php echo $this->config->item('skillll'); ?>img/icon/02.png"
							/>
						</a>
					</li>
					<li class="nav-item">
						<a
							href="https://github.com/mdbootstrap/bootstrap-material-design"
							class="nav-link  waves-effect"
						>
							<img
								src="<?php echo $this->config->item('skillll'); ?>img/icon/03.png"
							/>
						</a>
					</li>
					<li class="nav-item">
						<a
							href="https://github.com/mdbootstrap/bootstrap-material-design"
							class="nav-link   waves-effect"
						>
							<img
								class="point"
								src="<?php echo $this->config->item('skillll'); ?>img/icon/04.png"
							/>
							<span>150</span>
						</a>
					</li>
					<li class="nav-item">
						<img
							class="user"
							src="<?php echo $this->config->item('skillll'); ?>img/icon/user.png"
						/>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- Navbar -->
</header>

<!--Main Navigation-->
