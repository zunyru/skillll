<div class="col-xs-3 col-sm-3 col-md-4 col-md-offset-4 col-lg-3 mb-12 ipad mobile">
          
            <!--Card : Dynamic content wrapper-->
            <section class="text-left wow fadeIn">
              <div class="card my-radius ">
                <!--Card content-->
                <div class="card-body">
                  <div class="text-left mb-3">
                    <h4 class="text-name text-info">Popular Classes</h4>
                  </div>
                  <!--Title-->
                  <div class="mb-3">
                    <img class="img-fluid" src="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png" alt="skilll" srcset="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png">
                      <div class="float-left width-60"><span class="text-name-lable small-text ">Drawing Basics Class
                      By : Gorradesign</span></div> 
                      <div class="float-right">
                        <span class="text-name-lable small-text">1,200</span>
                        <img src="<?php echo $this->config->item('skillll'); ?>img/icon/group.png" >
                      </div>
                      <div class="clearfix"></div>
                  </div>    
                   
                  <div class="mb-3">
                    <img class="img-fluid" src="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png" alt="skilll" srcset="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png">
                    <div class="float-left width-60"><span class="text-name-lable small-text ">Drawing Basics Class
                        By : Gorradesign</span></div>
                    <div class="float-right">
                      <span class="text-name-lable small-text">1,200</span>
                      <img src="<?php echo $this->config->item('skillll'); ?>img/icon/group.png">
                    </div>
                    <div class="clearfix"></div>
                  </div>

                  <div class="mb-3">
                    <img class="img-fluid" src="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png" alt="skilll" srcset="<?php echo $this->config->item('skillll'); ?>img/demo/img01.png">
                    <div class="float-left width-60"><span class="text-name-lable small-text ">Drawing Basics Class
                        By : Gorradesign</span></div>
                    <div class="float-right">
                      <span class="text-name-lable small-text">1,200</span>
                      <img src="<?php echo $this->config->item('skillll'); ?>img/icon/group.png">
                    </div>
                    <div class="clearfix"></div>
                  </div>


                  <div class="text-center">
                  <a href="#">See All</a>
                  </div>
            
                </div>
              </div>
            </section>
            <!--/.Card : Dynamic content wrapper-->
        

        </div>