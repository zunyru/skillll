<div class="col-xs-6 col-sm-6 col-md-8 col-lg-6 mb-12">
          
            <!--Featured Post-->
            <div class=" mb-4 wow input-post-mini">
              <div class="form-group has-search ">
                <img class="p-3 form-control-feedback" src="<?php echo $this->config->item('skillll'); ?>img/icon/user2.png">
                <input  type="text" class="form-control post" placeholder="Post something.." >
              </div>
            </div>
            <!--/.Featured Post-->
          
            <!--Card-->
            <div class="card mb-4 wow" id="post-sec">
          
              <!--Card content-->
              <div class="card-body text-left">
                <button type="button" class="close" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
                <h5 class="text-name-lable text-left" id="postLabel">Create your Post</h5>
                <div class="form-group has-search mb-3">
                  <img class="p-3 form-control-feedback" src="<?php echo $this->config->item('skillll'); ?>img/icon/user2.png">
                  <textarea rows="1" type="text" id="input-post" class="form-control post-content" placeholder="Post something.."></textarea>
                </div>
              
                <div class="form-group">
                  <div class="file-loading">
                    <input type="file" class="file" id="test-upload" multiple data-theme="fas">
                  </div>
                  <div id="errorBlock" class="help-block"></div>
                </div>

                <div class="form-group col-12">
                  <div class="row">
                    <div class="col">
                      <select class="select-post">
                          <option>Nested option</option>
                          <option>Nested option</option>
                          <option>Nested option</option>
                          <option>Nested option Nested option Nested option Nested option</option>
                      </select>
                    </div>
                  <div class="col">
                    <button type="button" class="btn btn-primary post float-right">Post</button>
                  </div>
                  </div>
                  
                </div>
              
              </div>
          
            </div>
            <!--/.Card-->
          
            <!--Card-->
            <div class="card mb-4 wow fadeIn">
              <!--Card content-->
              <div class="card-body">
          
                <div class="media d-block d-md-flex ">
                  <img class="mb-3 mx-auto z-depth-1 rounded-circle" src="https://mdbootstrap.com/img/Photos/Avatars/img (30).jpg"
                    alt="Generic placeholder image" style="width: 60px;">
                  <div class="media-body text-md-left ml-md-3 ml-0">
                    <h5 class="mt-0 font-weight-bold">Mr.Winson
                    </h5>
                    <p>I will show you how to build the easy chair in your home...</p>
                    <img src="<?php echo $this->config->item('skillll'); ?>img/icon/public.png"><span class="text-name-lable ml-2">6 days ago</span>
                  </div>
                </div>
                <div class="post-contents">
                <iframe src="https://player.vimeo.com/video/76979871?embedparameter=value" width="100%" height="" frameborder="0"
                  allowfullscreen></iframe>
                </div>
                <div class="post-contents-detail p-2">
                  <div class="row">
                  <div class="col-3 ">
                  <img src="<?php echo $this->config->item('skillll'); ?>img/demo/shutterstock.png">
                  </div>
                  <div class="col-6">
                    <p class="fri">Furniture DIY course make it at home</p>
                    <p class="sec text-name-lable">He is questioned by U.S. customs officials,
                    and his scars and gang tattoos, marking him</p>
                  </div>
                  <div class="col-3">
                    <button class="btn btn-orange-c post">Free</button>
                  </div>
                  </div>
                </div>

                <div class="post-contents-comment p-2 mt-2">
                  <div class="row">
                    <div class="col-3 pr-0">
                      <span class="">Views 1,986</span>
                    </div>
                    <div class="col-2 px-0">
                      <img src="<?php echo $this->config->item('skillll'); ?>img/icon/07.png"><span class="pl-1">150</span>
                    </div>
                    <div class="col-2 px-0">
                      <img src="<?php echo $this->config->item('skillll'); ?>img/icon/06.png"><span class="pl-1">20</span>
                    </div>
                    <div class="col-2 px-0">
                      <img src="<?php echo $this->config->item('skillll'); ?>img/icon/05.png"><span class="pl-1">Share</span>
                    </div>
                    <div class="col-2 px-0">
                      <button class="btn btn-info post m-0">Follow</button>
                    </div>
                  </div>

                  <div class="row mt-2">
                    <div class="col-1 mr-2">
                      <img class="rounded-circle" src="https://mdbootstrap.com/img/Photos/Avatars/img (30).jpg"
                        alt="Generic placeholder image" style="width: 40px;">
                    </div>
                    <div class="col-6">
                      <input type="text" class="form-control comment">
                    </div>
                    <div class="col-4 mt-2">
                      <a href="#">Send</a>
                    </div>
                  </div>
                  <!-- comment -->
                  <div class="row mt-2">
                    <div class="col-1 mr-2">
                      <img class="rounded-circle" src="https://mdbootstrap.com/img/Photos/Avatars/img (30).jpg"
                        alt="Generic placeholder image" style="width: 40px;">
                    </div>
                    <div class="col-10 comment-text">
                      <div class="row ml-2">
                      He is questioned by U.S. customs officials, and his scars and gang tattoos, marking him as an assassin, do not go
                      unnoticed.
                      </div>
                      <div class="row mt-1">
                        <div class="col-2 px-0 like">
                          <span class="pl-1">3</span>
                          <img src="<?php echo $this->config->item('skillll'); ?>img/icon/07.png">
                        </div>
                        <div class="col-2 px-0 ">
                          6 days ago
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end comment -->
                  <!-- comment -->
                  <div class="row mt-2">
                    <div class="col-1 mr-2">
                      <img class="rounded-circle" src="https://mdbootstrap.com/img/Photos/Avatars/img (30).jpg"
                        alt="Generic placeholder image" style="width: 40px;">
                    </div>
                    <div class="col-10 comment-text">
                      <div class="row ml-2">
                        He is questioned by U.S. customs officials, and his scars and gang tattoos, marking him as an assassin, do not go
                        unnoticed.
                      </div>
                      <div class="row mt-1">
                        <div class="col-2 px-0 like">
                          <span class="pl-1">3</span>
                          <img src="<?php echo $this->config->item('skillll'); ?>img/icon/07.png">
                        </div>
                        <div class="col-2 px-0 ">
                          6 days ago
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end comment -->
                </div>
          
              </div>
          
            </div>
            <!--/.Card-->

             <!--Card-->
            <div class="card mb-4 wow fadeIn">
              <!--Card content-->
              <div class="card-body">
          
                <div class="media d-block d-md-flex ">
                  <img class="mb-3 mx-auto z-depth-1 rounded-circle" src="https://mdbootstrap.com/img/Photos/Avatars/img (30).jpg"
                    alt="Generic placeholder image" style="width: 60px;">
                  <div class="media-body text-md-left ml-md-3 ml-0">
                    <h5 class="mt-0 font-weight-bold">Mr.Winson
                    </h5>
                    <p>I will show you how to build the easy chair in your home...</p>
                    <img src="<?php echo $this->config->item('skillll'); ?>img/icon/public.png"><span class="text-name-lable ml-2">6 days ago</span>
                  </div>
                </div>
                <div class="post-contents">
                <iframe src="https://player.vimeo.com/video/76979871?embedparameter=value" width="100%" height="" frameborder="0"
                  allowfullscreen></iframe>
                </div>
                <div class="post-contents-detail p-2">
                  <div class="row">
                  <div class="col-3 ">
                  <img src="<?php echo $this->config->item('skillll'); ?>img/demo/shutterstock.png">
                  </div>
                  <div class="col-6">
                    <p class="fri">Furniture DIY course make it at home</p>
                    <p class="sec text-name-lable">He is questioned by U.S. customs officials,
                    and his scars and gang tattoos, marking him</p>
                  </div>
                  <div class="col-3">
                    <button class="btn btn-orange-c post">Free</button>
                  </div>
                  </div>
                </div>

                <div class="post-contents-comment p-2 mt-2">
                  <div class="row">
                    <div class="col-3 pr-0">
                      <span class="">Views 1,986</span>
                    </div>
                    <div class="col-2 px-0">
                      <img src="<?php echo $this->config->item('skillll'); ?>img/icon/07.png"><span class="pl-1">150</span>
                    </div>
                    <div class="col-2 px-0">
                      <img src="<?php echo $this->config->item('skillll'); ?>img/icon/06.png"><span class="pl-1">20</span>
                    </div>
                    <div class="col-2 px-0">
                      <img src="<?php echo $this->config->item('skillll'); ?>img/icon/05.png"><span class="pl-1">Share</span>
                    </div>
                    <div class="col-2 px-0">
                      <button class="btn btn-info post m-0">Follow</button>
                    </div>
                  </div>

                  <div class="row mt-2">
                    <div class="col-1 mr-2">
                      <img class="rounded-circle" src="https://mdbootstrap.com/img/Photos/Avatars/img (30).jpg"
                        alt="Generic placeholder image" style="width: 40px;">
                    </div>
                    <div class="col-6">
                      <input type="text" class="form-control comment">
                    </div>
                    <div class="col-4 mt-2">
                      <a href="#">Send</a>
                    </div>
                  </div>
                  <!-- comment -->
                  <div class="row mt-2">
                    <div class="col-1 mr-2">
                      <img class="rounded-circle" src="https://mdbootstrap.com/img/Photos/Avatars/img (30).jpg"
                        alt="Generic placeholder image" style="width: 40px;">
                    </div>
                    <div class="col-10 comment-text">
                      <div class="row ml-2">
                      He is questioned by U.S. customs officials, and his scars and gang tattoos, marking him as an assassin, do not go
                      unnoticed.
                      </div>
                      <div class="row mt-1">
                        <div class="col-2 px-0 like">
                          <span class="pl-1">3</span>
                          <img src="<?php echo $this->config->item('skillll'); ?>img/icon/07.png">
                        </div>
                        <div class="col-2 px-0 ">
                          6 days ago
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end comment -->
                  <!-- comment -->
                  <div class="row mt-2">
                    <div class="col-1 mr-2">
                      <img class="rounded-circle" src="https://mdbootstrap.com/img/Photos/Avatars/img (30).jpg"
                        alt="Generic placeholder image" style="width: 40px;">
                    </div>
                    <div class="col-10 comment-text">
                      <div class="row ml-2">
                        He is questioned by U.S. customs officials, and his scars and gang tattoos, marking him as an assassin, do not go
                        unnoticed.
                      </div>
                      <div class="row mt-1">
                        <div class="col-2 px-0 like">
                          <span class="pl-1">3</span>
                          <img src="<?php echo $this->config->item('skillll'); ?>img/icon/07.png">
                        </div>
                        <div class="col-2 px-0 ">
                          6 days ago
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end comment -->
                </div>
          
              </div>
          
            </div>
            <!--/.Card-->
          
            
          
          </div>