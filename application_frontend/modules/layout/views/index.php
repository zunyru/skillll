<?php $this->load->view('layout/header'); ?>

<body>
   <!--ZUN  -->
  <?php $this->load->view('layout/main-navigation'); ?>

  <!--Main layout-->
  <main class="mt-3 pt-5">
    <div class="container">

      <!--Section: Post-->
      <section class="mt-4">

        <!--Grid row-->
        <div class="row">   

          <!--Grid column-->
         <?php $this->load->view('layout/grid-column-1'); ?>
          <!--Grid column-->

          <!--Grid column-->
         <?php $this->load->view('layout/grid-column-2'); ?>
          <!--Grid column-->

          <!--Grid column-->
          <?php $this->load->view('layout/grid-column-3'); ?>
        <!--Grid row-->

      </section>
      <!--Section: Post-->

    </div>
  </main>
  <!--Main layout-->

  <!--Footer-->
  <footer class="page-footer text-center font-small mdb-color darken-2 mt-4 wow fadeIn">

  </footer>
  <!--/.Footer-->

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="<?php echo $this->config->item('skillll'); ?>js/jquery-3.4.0.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo $this->config->item('skillll'); ?>js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo $this->config->item('skillll'); ?>js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo $this->config->item('skillll'); ?>js/mdb.min.js"></script>
  <!-- Initializations -->
  <script type="text/javascript" src="<?php echo $this->config->item('skillll'); ?>js/select2.full.min.js"></script>

  <script src="<?php echo $this->config->item('skillll'); ?>dist/bootstrap-fileinput/js/plugins/piexif.js" type="text/javascript"></script>
  <script src="<?php echo $this->config->item('skillll'); ?>dist/bootstrap-fileinput/js/plugins/sortable.js" type="text/javascript"></script>
  <script src="<?php echo $this->config->item('skillll'); ?>dist/bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
  <script src="<?php echo $this->config->item('skillll'); ?>dist/bootstrap-fileinput/js/locales/fr.js" type="text/javascript"></script>
  <script src="<?php echo $this->config->item('skillll'); ?>dist/bootstrap-fileinput/js/locales/es.js" type="text/javascript"></script>
  <script src="<?php echo $this->config->item('skillll'); ?>dist/bootstrap-fileinput/themes/fas/theme.js" type="text/javascript"></script>
  <script src="<?php echo $this->config->item('skillll'); ?>dist/bootstrap-fileinput/themes/explorer-fas/theme.js" type="text/javascript"></script>
 
  <script type="text/javascript">
    // Animations initialization
    new WOW().init();
    
    $('#post-sec').hide()
    $(".post").click(function () {
      $('#post-sec').show();
      $('.input-post-mini').hide()
      $('#input-post').focus();
    });

    $('#post-sec .close').click(function(){
      $('#post-sec').hide()
      $('.input-post-mini').show()
    })

    var textarea = document.querySelector('textarea');

      textarea.addEventListener('keypress', autosize);

      function autosize() {
        var el = this;
        var h = el.scrollHeight;
        setTimeout(function () {
          el.style.cssText = 'height:auto; padding:0';
          el.style.cssText = 'height:' + h + 'px';
        }, 0);
      }

    $('.select-post').select2({
      placeholder: "Select tag course",
      allowClear: false
    });  


     var $el1 = $("#test-upload");
      $el1.fileinput({
        theme: 'fas',
        allowedFileExtensions: ['jpg', 'png', 'gif'],
        allowedFileTypes: ['image', 'video', 'flash'],
        uploadUrl: "/file-upload-batch/2",
        uploadAsync: false,
        showCaption: false,
        showUpload: false, // hide upload button
        showRemove: false, // hide remove button
        overwriteInitial: false, // append files to initial preview
        //minFileCount: 1,
        maxFileCount: 5,
        initialPreviewAsData: true,
        browseOnZoneClick: true,
        showBrowse: false,
        previewClass: "bg-light-c",
        dropZoneTitle : "+",
        dropZoneClickTitle : "<br><small>Add Photo or Video</<small>",
        uploadExtraData: function (previewId, index) {
          return { key: index };
        },
      }).on("filebatchselected", function (event, files) {
        $el1.fileinput("upload");
      });
     


  </script>
</body>

</html>
