</section>
<!-- Essential javascripts for application to work-->
<script src="<?php echo $this->config->item('vali'); ?>js/jquery-3.2.1.min.js"></script>
<script src="<?php echo $this->config->item('vali'); ?>js/popper.min.js"></script>
<script src="<?php echo $this->config->item('vali'); ?>js/bootstrap.min.js"></script>
<script src="<?php echo $this->config->item('vali'); ?>js/main.js"></script>
<!-- The javascript plugin to display page loading on top-->
<script src="<?php echo $this->config->item('vali'); ?>js/plugins/pace.min.js"></script>
